# directChillCasting

  Set of solvers/utilities used for modelling direct-chill casting processes at the [LKR Leichtmetallkompetenzzentrum Ranshofen]( https://www.ait.ac.at/ueber-das-ait/center/center-for-transport-technologies/lkr-leichtmetallkompetenzzentrum-ranshofen).

  The main goal of this repository is to allow public access to the customised CFD solvers/utilities we use for our research.

## About this repository

  This repository was initially started as a fork from [directChillFoam]( https://github.com/blebon/directChillFoam), that contains the developments made for modelling direct-chill (DC) casting by Bruno Lebon at BCAST, Brunel University London using OpenFOAM.

  Additional features and custom developments arising from our research work will be gradually integrated.


## Installation  

  This project is actively maintained for usage with OpenFOAM-10 from within a GNU-Linux (or WSL) environment

  1. In case of Windows (only), for installing WSL and setting up the desired Linux distro we recommend following the instructions from [Microsoft]( https://learn.microsoft.com/en-us/windows/wsl/install)

  2. For installing OpenFOAM-10 we recommend following the instructions from the [OpenFOAM Foundation]( https://openfoam.org/release/10/) 

  3. Download this project from a Linux-terminal (with ```git```)  
  a. EITHER using HTTPS  
    ```cd <installation-path>```  
    ```git clone https://gitlab.com/ait-lkr/directchillcasting.git ```  
  b. OR using SSH  
    ``` cd <installation-path>```  
    ```git clone git@gitlab.com:ait-lkr/directchillcasting.git ```  

  4. Load OpenFOAM environment, typically  
  ```source /opt/openfoam10/etc/bashrc```  

  5. Compile all the utilities  
  ```cd <installation-path>/directchillcasting```  
  ```./Allwmake```

  You can anytime clean your local installation using  
  ```cd <installation-path>/directchillcasting```  
  ```wclean all```


## Contributions  

  Everyone is welcome to contribute to this repository.  
  Please, open an [issue]( https://gitlab.com/ait-lkr/directchillcasting/-/issues)
  if your find any bug or improvement suggestion.  
  You can also create a [merge request]( https://gitlab.com/ait-lkr/directchillcasting/-/merge_requests)
  if you already have a code suggestion.

  Upstream contributions to the projects [directChillFoam]( https://github.com/blebon/directChillFoam) and [OpenFOAM]( https://openfoam.org) are susceptible of individual assessment.


## License  

  This open source project is released under [GNU-GPLv3](https://www.gnu.org/licenses/quick-guide-gplv3.html) license.  
  See the file [LICENSE](LICENSE) for more details.


## Acknowledgement  

  This work has been financially supported by the Federal Ministry for Climate Protection, Environment, Energy, Mobility, Innovation and Technology (BMK) and the Austrian Research Promotion Agency (FFG) as part of Production and Material 2022, project [opt1mus](https://www.ait.ac.at/en/research-topics/numerical-simulation/projects/opt1mus)

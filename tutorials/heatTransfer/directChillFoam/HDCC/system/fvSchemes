/*--------------------------------*- C++ -*----------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     | Website:  https://openfoam.org
    \\  /    A nd           | Version:  9
     \\/     M anipulation  |
\*---------------------------------------------------------------------------*/
FoamFile
{
    format          ascii;
    class           dictionary;
    location        "system";
    object          fvSchemes;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#include "transientSettings"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //


ddtSchemes
{
    default         $DDT_SCHEME;
}

gradSchemes
{
    default         Gauss linear;
    grad(h)         cellLimited leastSquares 1;
}

divSchemes
{
    default         none;
    div(((rho*nuEff)*dev2(T(grad(U))))) Gauss linear;

    div(phi,U)      Gauss linearUpwindV grad(U);
    div(phi,h)      Gauss upwind;
    div(phi,K)      Gauss linear;
    div(phi,alpha)  Gauss linear;
    div(phi,C)      Gauss linear;

    div(phi,n)      Gauss limitedLinear 1;
}

laplacianSchemes
{
    default         Gauss linear limited corrected 0.7;
}

interpolationSchemes
{
    default         linear;
}

snGradSchemes
{
    default         limited corrected 0.7;
}

wallDist
{
    method meshWave;
}


// ************************************************************************* //

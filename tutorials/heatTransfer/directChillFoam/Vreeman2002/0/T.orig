/*--------------------------------*- C++ -*----------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     | Website:  https://openfoam.org
    \\  /    A nd           | Version:  9
     \\/     M anipulation  |
\*---------------------------------------------------------------------------*/
FoamFile
{
    format      ascii;
    class       volScalarField;
    location    "0";
    object      T;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#include "$PWD/system/caseParameters"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

dimensions      [0 0 0 1 0 0 0];

internalField   uniform $tInitial;

boundaryField
{
    free-surface
    {
        type            fixedValue;
        value           uniform $tInitial;
    }

    "(ceramic|hot-top)"
    {
        type            zeroGradient;
    }

    "(mould|air-gap)"
    {
        type            mouldHTC;
        mode            coefficient;
        h1              constant 1250.0;
        h2              constant 40.0;
        contactField    "melt1_alpha1";
        Ta              constant $waterTemp;
        kappaMethod     fluidThermo;
        relaxation      1.0;
        value           uniform $waterTemp;
    }

    water-film
    {
        type            WeckmannNiessenHTC;
        d               0.255;
        Tsat            373.15;
        qDotWater       constant 0.0015;
        Twater          constant $waterTemp;
        Ta              constant $waterTemp;
        kappaMethod     fluidThermo;
        relaxation      0.3;

        value           uniform $waterTemp;
    }

    "(ram|bottom)"
    {
        type            zeroGradient;
    }

    #includeEtc "caseDicts/setConstraintTypes"
}

// ************************************************************************* //

/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     | Website:  https://openfoam.org
    \\  /    A nd           | Copyright (C) 2012-2022 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

\*---------------------------------------------------------------------------*/

#include "limitScalar.H"
#include "volFields.H"
#include "addToRunTimeSelectionTable.H"

// * * * * * * * * * * * * * * Static Data Members * * * * * * * * * * * * * //

namespace Foam
{
namespace fv
{
    defineTypeNameAndDebug(limitScalar, 0);
    addToRunTimeSelectionTable
    (
        fvConstraint,
        limitScalar,
        dictionary
    );
}
}


// * * * * * * * * * * * * * Private Member Functions  * * * * * * * * * * * //

void Foam::fv::limitScalar::readCoeffs()
{
    Smin_ = coeffs().lookup<scalar>("min");
    Smax_ = coeffs().lookup<scalar>("max");
    fieldName_ = coeffs().lookup<word>("field");
}


// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

Foam::fv::limitScalar::limitScalar
(
    const word& name,
    const word& modelType,
    const dictionary& dict,
    const fvMesh& mesh
)
:
    fvConstraint(name, modelType, dict, mesh),
    set_(coeffs(), mesh),
    Smin_(-vGreat),
    Smax_(vGreat),
    fieldName_(word::null)
{
    readCoeffs();
}


// * * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * //

Foam::wordList Foam::fv::limitScalar::constrainedFields() const
{

    return wordList(1, fieldName_);
}


bool Foam::fv::limitScalar::constrain(volScalarField& S) const
{
    const labelList& cells = set_.cells();

    if (S.name() != fieldName_ )
    {
        return cells.size();
    }

    scalarField& Sc = S.primitiveFieldRef();

    forAll(cells, i)
    {
        const label celli = cells[i];
        Sc[celli] = max(min(Sc[celli], Smax_), Smin_);
    }

    // Handle boundaries in the case of 'all'
    if (set_.selectionMode() == fvCellSet::selectionModeType::all)
    {
        volScalarField::Boundary& Sbf = S.boundaryFieldRef();

        forAll(Sbf, patchi)
        {
            fvPatchScalarField& Sp = Sbf[patchi];

            if (!Sp.fixesValue())
            {
                forAll(Sp, facei)
                {
                    Sp[facei] = max(min(Sp[facei], Smax_), Smin_);
                }
            }
        }
    }

    return cells.size();
}


bool Foam::fv::limitScalar::movePoints()
{
    set_.movePoints();
    return true;
}


void Foam::fv::limitScalar::topoChange(const polyTopoChangeMap& map)
{
    set_.topoChange(map);
}


void Foam::fv::limitScalar::mapMesh(const polyMeshMap& map)
{
    set_.mapMesh(map);
}


void Foam::fv::limitScalar::distribute(const polyDistributionMap& map)
{
    set_.distribute(map);
}


bool Foam::fv::limitScalar::read(const dictionary& dict)
{
    if (fvConstraint::read(dict))
    {
        set_.read(coeffs());
        readCoeffs();
        return true;
    }
    else
    {
        return false;
    }
}


// ************************************************************************* //
